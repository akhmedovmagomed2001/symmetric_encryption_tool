package security;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static security.Constants.CYPHER_TRANSFORMATION;

public class SecurityUtils {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    public static EncryptionData createEncryptionData() {
        Key secretKey;
        try {
            secretKey = getRandomKey(256);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        byte[] initialVector = generateInitialVector();

        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        return new EncryptionData(secretKey, ivParameterSpec);
    }

    public static Key getRandomKey(int bitCount) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(bitCount, SECURE_RANDOM);
        return keyGenerator.generateKey();
    }

    public static Key getExistingKey(byte[] key) {
        String algorithm = "AES";
        return new SecretKeySpec(key, algorithm);
    }

    public static byte[] generateInitialVector() {
        byte[] result = new byte[16];
        SECURE_RANDOM.nextBytes(result);
        return result;
    }

}
