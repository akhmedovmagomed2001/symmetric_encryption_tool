package security;

import org.apache.commons.cli.*;

import static security.Constants.TOOL_NAME;

public class CmdContext {

    private final Options options = createCmdOptions();
    private final CommandLineParser parser = new DefaultParser();
    private final HelpFormatter formatter = new HelpFormatter();

    private CommandLine commandLine;

    private CmdContext() {
    }

    public static CmdContext createCmdContext(String[] args) {
        CmdContext cmdContext = new CmdContext();
        Options commandLineOptions = cmdContext.getOptions();

        try {
            CommandLineParser commandLineParser = cmdContext.getParser();
            cmdContext.commandLine = commandLineParser.parse(commandLineOptions, args);
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
            cmdContext.printHelp();
            System.exit(1);
        }

        return cmdContext;
    }

    public void printHelp() {
        HelpFormatter helpFormatter = this.getFormatter();
        Options commandLineOptions = this.getOptions();
        helpFormatter.printHelp(TOOL_NAME, commandLineOptions);
    }

    private static Options createCmdOptions() {
        Options options = new Options();

        Option modeOption = new Option("m", "mode", true, "Mode of the program");
        modeOption.setRequired(true);
        options.addOption(modeOption);

        Option outputOption = new Option("o", "output", true, "Output");
        modeOption.setRequired(true);
        options.addOption(outputOption);

        Option inputOption = new Option("i", "input", true, "Input");
        inputOption.setRequired(true);
        options.addOption(inputOption);

        Option inputIVOption = new Option("iv", "iv", true, "Initialization Vector path");
        options.addOption(inputIVOption);

        Option inputKeyOption = new Option("k", "key", true, "Input key");
        options.addOption(inputKeyOption);
        return options;
    }

    public CommandLineParser getParser() {
        return parser;
    }

    public HelpFormatter getFormatter() {
        return formatter;
    }

    public Options getOptions() {
        return options;
    }

    public CommandLine getCommandLine() {
        return commandLine;
    }
}
