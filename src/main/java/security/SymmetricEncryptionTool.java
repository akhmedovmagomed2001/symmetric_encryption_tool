package security;

import org.apache.commons.cli.CommandLine;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import static security.Constants.*;
import static security.GeneralUtils.*;
import static security.SecurityUtils.createEncryptionData;
import static security.SecurityUtils.getExistingKey;

public class SymmetricEncryptionTool {

    public static void main(String[] args) {
        CmdContext cmdContext = CmdContext.createCmdContext(args);

        CommandLine cmd = cmdContext.getCommandLine();
        String modeOptionValue = cmd.getOptionValue("m");
        if (modeOptionValue.equals("D")) {
            validateOptions(cmd, DECRYPT_REQUIRED_OPTIONS);
            decrypt(cmd);
        } else {
            validateOptions(cmd, ENCRYPT_REQUIRED_OPTIONS);
            encrypt(cmd);
        }
    }

    private static void encrypt(CommandLine commandLine) {
        String inputFilename = commandLine.getOptionValue("i");
        byte[] inputBytes = readFromFile(inputFilename);

        EncryptionData encryptionData = createEncryptionData();
        try {
            Cipher cipher = Cipher.getInstance(CYPHER_TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, encryptionData.getKey(), encryptionData.getIv());

            byte[] encryptedText = cipher.doFinal(inputBytes);
            String outputFilename = commandLine.getOptionValue("o");
            writeToFile(encryptedText, outputFilename);
            writeToFile(encryptionData.getIv().getIV(), IV_FILENAME);
            writeToFile(encryptionData.getKey().getEncoded(), KEY_FILENAME);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                 InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException(e);
        }
    }


    private static void decrypt(CommandLine commandLine) {
        String keyFilename = commandLine.getOptionValue("key");
        byte[] keyBytes = readFromFile(keyFilename);
        Key key = getExistingKey(keyBytes);

        String ivFilename = commandLine.getOptionValue("iv");
        byte[] iv = readFromFile(ivFilename);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        try {
            Cipher cipher = Cipher.getInstance(CYPHER_TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec);

            String inputFilename = commandLine.getOptionValue("i");
            byte[] inputBytes = readFromFile(inputFilename);
            byte[] decryptedTextBytes = cipher.doFinal(inputBytes);

            String outputFilename = commandLine.getOptionValue("o");
            writeToFile(decryptedTextBytes, outputFilename);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException |
                 InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException(e);
        }
    }
}
