package security;

import javax.crypto.spec.IvParameterSpec;
import java.security.Key;

public class EncryptionData {
    private final Key key;
    private final IvParameterSpec iv;

    public EncryptionData(Key key, IvParameterSpec iv) {
        this.key = key;
        this.iv = iv;
    }

    public Key getKey() {
        return key;
    }

    public IvParameterSpec getIv() {
        return iv;
    }

    @Override
    public String toString() {
        String keyStr = toBytesString(key.getEncoded());
        String ivStr = toBytesString(iv.getIV());

        return "EncryptionData:\n" +
                "Key: " + keyStr + "\n" +
                "IV: " + ivStr;
    }

    private static String toBytesString(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(String.format("%02X ", b));
        }
        return result.toString();
    }
}
