package security;

import java.util.Arrays;
import java.util.List;

public class Constants {

    public static final String CYPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";
    public static final List<String> ENCRYPT_REQUIRED_OPTIONS = Arrays.asList("m", "i", "o");
    public static final List<String> DECRYPT_REQUIRED_OPTIONS = Arrays.asList("m", "i", "o", "iv", "k");
    public static final String KEY_FILENAME = "key.hex";
    public static final String IV_FILENAME = "iv.hex";
    public static final String TOOL_NAME = "Symmetric-encryption Tool";

}
