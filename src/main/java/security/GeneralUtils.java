package security;

import org.apache.commons.cli.CommandLine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class GeneralUtils {

    public static void validateOptions(CommandLine commandLine, List<String> requiredOptions) {
        boolean requiredOptionsArePresent = requiredOptions.stream().allMatch(commandLine::hasOption);
        if (!requiredOptionsArePresent) {
            String requiredOptionsJoin = String.join(" ", requiredOptions);
            String message = String.format("Following options must be specified: '%s'", requiredOptionsJoin);
            throw new RuntimeException(message);
        }
    }

    public static byte[] readFromFile(String filename) {
        File resourceFile = new File(filename);
        byte[] result;

        try (FileInputStream fileInputStream = new FileInputStream(resourceFile)) {
            result = new byte[fileInputStream.available()];
            fileInputStream.read(result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    public static void writeToFile(byte[] data, String filename) {
        File resourceFile = new File(filename);
        try {
            resourceFile.delete();
            resourceFile.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try (FileOutputStream fileOutputStream = new FileOutputStream(resourceFile)) {
            fileOutputStream.write(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
