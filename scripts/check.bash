cd ..
gradle clean jar
cd data
java -jar ../build/libs/SymmetricEncryptionTool-1.0.jar -m E -i AliceCH1.txt -o AliceCH1_ENC.hex
java -jar ../build/libs/SymmetricEncryptionTool-1.0.jar -m D -i AliceCH1_ENC.hex -iv iv.hex -k key.hex -o AliceCH1_DEC.txt
